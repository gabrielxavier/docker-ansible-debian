FROM debian:stretch-slim
LABEL maintainer = "Gabriel Xavier | gabrielxavier.com"

ENV DEBIAN_FRONTEND="noninteractive"
ENV TZ=America/Sao_Paulo
ENV ANSIBLE_CONFIG=/ansible/ansible.cfg

RUN set -ex; \
    \
    appInstall=" \
        curl \
        default-libmysqlclient-dev \
        gettext-base \
        git \
        mysql-client \
        openssh-client \
        python \
        python-pip \
        tar \
    "; \
    pipInstall=" \
        ansible \
        boto \
        boto3 \
        cryptography \
        MySQL-python \
        pycrypto \
        pywinrm \
    "; \
    buildDeps=" \
        gcc \
        python-dev \
    "; \
    pipDeps=" \
        setuptools \
    "; \
    \
    apt update -q; \
    apt install --no-install-recommends -qy $buildDeps $appInstall; \
    \
    pip install --no-cache-dir $pipDeps; \
    pip install --no-cache-dir $pipInstall; \
    \
    pip uninstall -y $pipDeps; \
    apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*; \
    apt-get purge -y --auto-remove $buildDeps
    
COPY ./ansible /ansible

WORKDIR /ansible

CMD ["ansible-connection"]
