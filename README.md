[![Build status](https://gitlab.com/gabrielxavier/docker-ansible-debian/badges/master/build.svg)](https://gitlab.com/gabrielxavier/docker-ansible-debian/commits/master)
### Docker image with Ansible

#### Last versions of:

- Python 2.7
- Ansible 2.6

[DockerHub](https://hub.docker.com/r/gabxav/docker-ansible/)
